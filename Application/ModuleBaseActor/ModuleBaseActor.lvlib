﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung.
Released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Messages" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Logging" Type="Folder">
			<Item Name="LoggingLoopOnOff Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/LoggingLoopOnOff Msg/LoggingLoopOnOff Msg.lvclass"/>
			<Item Name="SetLoggingDelay Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetLoggingDelay Msg/SetLoggingDelay Msg.lvclass"/>
			<Item Name="SetLoggingBasePath Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetLoggingBasePath Msg/SetLoggingBasePath Msg.lvclass"/>
			<Item Name="EnableLog Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/EnableLog Msg/EnableLog Msg.lvclass"/>
			<Item Name="SetLoggingMode Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetLoggingMode Msg/SetLoggingMode Msg.lvclass"/>
		</Item>
		<Item Name="ConfigureModule Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/ConfigureModule Msg/ConfigureModule Msg.lvclass"/>
		<Item Name="ConnectDataSrcFromConfig Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/ConnectDataSrcFromConfig Msg/ConnectDataSrcFromConfig Msg.lvclass"/>
		<Item Name="DataLoopOnOff Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/DataLoopOnOff Msg/DataLoopOnOff Msg.lvclass"/>
		<Item Name="DataSourceChanged Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/DataSourceChanged Msg/DataSourceChanged Msg.lvclass"/>
		<Item Name="SetModuleName Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetModuleName Msg/SetModuleName Msg.lvclass"/>
		<Item Name="SaveSettings Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SaveSettings Msg/SaveSettings Msg.lvclass"/>
		<Item Name="SetOpenModules Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetOpenModules Msg/SetOpenModules Msg.lvclass"/>
		<Item Name="ShowFrontPanel Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/ShowFrontPanel Msg/ShowFrontPanel Msg.lvclass"/>
		<Item Name="RequestDataNotifiers Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/RequestDataNotifiers Msg/RequestDataNotifiers Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="RequestLoggingConfig Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/RequestLoggingConfig Msg/RequestLoggingConfig Msg.lvclass"/>
		<Item Name="SetDataNotifier Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/SetDataNotifier Msg/SetDataNotifier Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="RequestModuleHasLog Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/RequestModuleHasLog Msg/RequestModuleHasLog Msg.lvclass"/>
		<Item Name="Write AutoConnect DataSrc Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/Write AutoConnect DataSrc Msg/Write AutoConnect DataSrc Msg.lvclass"/>
		<Item Name="UpdateDataLoopGui Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/UpdateDataLoopGui Msg/UpdateDataLoopGui Msg.lvclass"/>
		<Item Name="RequestPlotInfo Msg.lvclass" Type="LVClass" URL="../../ModuleBaseActor Messages/RequestPlotInfo Msg/RequestPlotInfo Msg.lvclass"/>
	</Item>
	<Item Name="ModuleBaseActor.lvclass" Type="LVClass" URL="../ModuleBaseActor.lvclass"/>
	<Item Name="ModuleSettings.lvclass" Type="LVClass" URL="../ModuleSettings/ModuleSettings.lvclass"/>
</Library>
