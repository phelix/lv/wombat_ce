﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="BeamshapeGui" Type="Folder">
		<Item Name="Messages" Type="Folder">
			<Item Name="WriteBeamshapeContour Msg.lvclass" Type="LVClass" URL="../BeamshapeGui Messages/WriteBeamshapeContour Msg/WriteBeamshapeContour Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="ScaleBeamshapeGraph Msg.lvclass" Type="LVClass" URL="../BeamshapeGui Messages/ScaleBeamshapeGraph Msg/ScaleBeamshapeGraph Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="Autodetect OnOff Msg.lvclass" Type="LVClass" URL="../BeamshapeGui Messages/Autodetect OnOff Msg/Autodetect OnOff Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="UnhideWindow Msg.lvclass" Type="LVClass" URL="../BeamshapeGui Messages/UnhideWindow Msg/UnhideWindow Msg.lvclass"/>
		</Item>
		<Item Name="BeamshapeGui.lvclass" Type="LVClass" URL="../BeamshapeGui/BeamshapeGui.lvclass"/>
	</Item>
	<Item Name="ShotAberrationRecorder" Type="Folder">
		<Item Name="Messages" Type="Folder">
			<Item Name="FlushBuffer Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/FlushBuffer Msg/FlushBuffer Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="OnBGBufferSizeChange Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/OnBGBufferSizeChange Msg/OnBGBufferSizeChange Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="OnSaveLastDelta Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/OnSaveLastDelta Msg/OnSaveLastDelta Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="OnShotBufferSizeChange Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/OnShotBufferSizeChange Msg/OnShotBufferSizeChange Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="OnUpdatePreview Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/OnUpdatePreview Msg/OnUpdatePreview Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="RemoveLatestSample Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/RemoveLatestSample Msg/RemoveLatestSample Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="SetRecordingMode Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/SetRecordingMode Msg/SetRecordingMode Msg.lvclass">
				<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
			</Item>
			<Item Name="ShowFrontpanel Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/ShowFrontpanel Msg/ShowFrontpanel Msg.lvclass"/>
			<Item Name="WriteGradientFieldToBuffer Msg.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder Messages/WriteGradientFieldToBuffer Msg/WriteGradientFieldToBuffer Msg.lvclass"/>
		</Item>
		<Item Name="ShotAberrationRecorder.lvclass" Type="LVClass" URL="../ShotAberrationsRecorder/ShotAberrationRecorder.lvclass"/>
	</Item>
	<Item Name="Messages" Type="Folder">
		<Item Name="LoadBeamshape Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/LoadBeamshape Msg/LoadBeamshape Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="LoadReference Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/LoadReference Msg/LoadReference Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="NrOfShiftAVGChange Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/NrOfShiftAVGChange Msg/NrOfShiftAVGChange Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="OpenBeamshapeGui Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/OpenBeamshapeGui Msg/OpenBeamshapeGui Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="OpenShotAbberationRecorderPanel Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/OpenShotAbberationRecorderPanel Msg/OpenShotAbberationRecorderPanel Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="RemoveTilt Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/RemoveTilt Msg/RemoveTilt Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="SaveBeamshape Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/SaveBeamshape Msg/SaveBeamshape Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="SetAutodetectBeamshape Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/SetAutodetectBeamshape Msg/SetAutodetectBeamshape Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="SetDisplayPalette Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/SetDisplayPalette Msg/SetDisplayPalette Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="SetIgnoreTilt Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/SetIgnoreTilt Msg/SetIgnoreTilt Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="UpdateBeamshapeFromControls Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/UpdateBeamshapeFromControls Msg/UpdateBeamshapeFromControls Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Write Beamshape Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/Write Beamshape Msg/Write Beamshape Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Write BeamshapeDetectionArray Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/Write BeamshapeDetectionArray Msg/Write BeamshapeDetectionArray Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="WriteShiftsAVGStatus Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/WriteShiftsAVGStatus Msg/WriteShiftsAVGStatus Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="SetErodeOnOff Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/SetErodeOnOff Msg/SetErodeOnOff Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="DisplayBeamShapeOnGUI Msg.lvclass" Type="LVClass" URL="../../AnalysisActor Messages/DisplayBeamShapeOnGUI Msg/DisplayBeamShapeOnGUI Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
	</Item>
	<Item Name="AnalysisActor.lvclass" Type="LVClass" URL="../AnalysisActor.lvclass"/>
</Library>
