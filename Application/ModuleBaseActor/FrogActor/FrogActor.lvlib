﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Messages" Type="Folder">
		<Item Name="Load Calibration Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Load Calibration Msg/Load Calibration Msg.lvclass"/>
		<Item Name="Set central wavelength Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set central wavelength Msg/Set central wavelength Msg.lvclass"/>
		<Item Name="Set delta tau Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set delta tau Msg/Set delta tau Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set dG Min Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set dG Min Msg/Set dG Min Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set display palette Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set display palette Msg/Set display palette Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set Filter OnOff Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set Filter OnOff Msg/Set Filter OnOff Msg.lvclass"/>
		<Item Name="Set fit time Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set fit time Msg/Set fit time Msg.lvclass"/>
		<Item Name="Set lambda delta lambda Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set lambda delta lambda Msg/Set lambda delta lambda Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set lambda offset Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set lambda offset Msg/Set lambda offset Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set max iterations Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set max iterations Msg/Set max iterations Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set power calibration Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set power calibration Msg/Set power calibration Msg.lvclass"/>
		<Item Name="Set Reconstruct Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set Reconstruct Msg/Set Reconstruct Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set Reconstruction Method Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set Reconstruction Method Msg/Set Reconstruction Method Msg.lvclass"/>
		<Item Name="Set remove background Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set remove background Msg/Set remove background Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set select ROI Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set select ROI Msg/Set select ROI Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set size Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set size Msg/Set size Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Set threshold value Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/Set threshold value Msg/Set threshold value Msg.lvclass"/>
		<Item Name="SetResult Msg.lvclass" Type="LVClass" URL="../../FrogActor Messages/SetResult Msg/SetResult Msg.lvclass">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
	</Item>
	<Item Name="FrogActor.lvclass" Type="LVClass" URL="../FrogActor.lvclass"/>
</Library>
