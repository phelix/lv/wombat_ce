﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Messages" Type="Folder">
		<Item Name="Set BG Range Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set BG Range Msg/Set BG Range Msg.lvclass"/>
		<Item Name="Set Analysis Range Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Analysis Range Msg/Set Analysis Range Msg.lvclass"/>
		<Item Name="Set Set Analysis Range Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Set Analysis Range Msg/Set Set Analysis Range Msg.lvclass"/>
		<Item Name="Set Average Results Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Average Results Msg/Set Average Results Msg.lvclass"/>
		<Item Name="Set Number of Averages Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Number of Averages Msg/Set Number of Averages Msg.lvclass"/>
		<Item Name="Set Spec Name (Power Calibration) Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Spec Name (Power Calibration) Msg/Set Spec Name (Power Calibration) Msg.lvclass"/>
		<Item Name="Set Remove BG Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set Remove BG Msg/Set Remove BG Msg.lvclass"/>
		<Item Name="Set BG Path Msg.lvclass" Type="LVClass" URL="../../SpecAnalysisActor Messages/Set BG Path Msg/Set BG Path Msg.lvclass"/>
	</Item>
	<Item Name="SpecAnalysisActor.lvclass" Type="LVClass" URL="../SpecAnalysisActor.lvclass"/>
</Library>
